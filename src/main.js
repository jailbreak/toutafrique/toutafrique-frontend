import App from "./App.svelte"
import HMR from "@sveltech/routify/hmr"

const app = HMR(App, { target: document.body }, "routify-app")

window.Plotly.setPlotConfig({locale: 'fr'})

export default app
